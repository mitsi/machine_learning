import random
import timeit

from theano.gradient import np

from morpion import nn
from morpion.nn import *


class IA:
    observe = 200
    buffer = 20000
    batchSize = 100
    divide_epsilon = 10000
    NUM_INPUT = 9
    GAMMA = 0.25

    def __init__(self, name, symbol, isDummy=False, isHuman = False, noUsingLearning = False):
        #--------
        # Game
        #--------
        self.winGameVectors = []
        self.name = name
        self.symbol = symbol
        self.isDummy = isDummy
        self.isHuman = isHuman
        self.nbWin = 0
        self.noUsingLearning = noUsingLearning

        #--------
        # IA
        #--------
        self.tmp_state = None
        self.tmp_action = None
        self.start_time = timeit.default_timer()
        self.filename = "morpion_IA_data_collect"
        self.data_collect = []
        self.loss_log = []
        self.epsilon = 0.7
        self.replay = []
        self.nn_param = [9, 150]
        self.currentHitNumber = 0
        self.minHitNUmber = 100
        self.model = neural_net(self.NUM_INPUT, self.nn_param, "./h5/5000.h5")
        # self.model = neural_net(self.NUM_INPUT, self.nn_param)

    def play(self, playable_position, state = None, total_game = None):

        self.tmp_state = np.asarray(state).reshape(1, self.NUM_INPUT)
        # Choose an action.
        randomNb = random.random()
        if self.noUsingLearning:
            self.tmp_action = self.contains(state)
        elif self.isHuman:
            print(playable_position)
            self.tmp_action = int(input("index"))  # random
        elif randomNb < self.epsilon or total_game < self.observe or self.isDummy:
            self.tmp_action = playable_position[random.randint(0, len(playable_position)-1)]  # random
        else:
            print("predict")
            # Get Q values for each action.
            qval = self.model.predict(self.tmp_state, batch_size=1)
            self.tmp_action = (np.argmax(qval))  # best
        return self.tmp_action

    def callbackGameStateChange(self, reward, new_state, total_frame):
        if self.isDummy or self.isHuman:
            return
        # Take action, observe new state and get our treat.
        self.currentHitNumber = total_frame
        new_state = np.asarray(new_state).reshape(1, self.NUM_INPUT)
        # Experience replay storage.
        self.replay.append((self.tmp_state, self.tmp_action, reward, new_state))

        # If we're done observing, start training.
        if total_frame > self.observe:

            # If we've stored enough in our buffer, pop the oldest.
            if len(self.replay) > self.buffer:
                self.replay.pop(0)

            # Randomly sample our experience replay memory
            minibatch = random.sample(self.replay, self.batchSize)

            # Get training values.
            X_train, y_train = nn.process_minibatch(minibatch, self.model, self.GAMMA, self.NUM_INPUT)
            # Train the model on this batch.
            history = LossHistory()
            self.model.fit(
                X_train, y_train, batch_size=self.batchSize,
                nb_epoch=1, verbose=0, callbacks=[history]
            )
            self.loss_log.append(history.losses)

        # # Update the starting state with S'.
        # state = new_state

        # Decrement epsilon over time.
        if self.epsilon > 0.1 and total_frame > self.observe:
            self.epsilon -= (1 / self.divide_epsilon)

        # Save the model every 25 000 frames.
        if total_frame % 1000 == 0 and total_frame > 0:
            self.model.save_weights("h5/"+str(total_frame) + '.h5',
                                    overwrite=True)
            print("Saving model %s - %d" % (self.filename, total_frame))

        # Log results after we're done all frames.
        log_results(self.filename, self.data_collect, self.loss_log)

    def win(self):
        self.nbWin += 1
        return 50

    def loose(self):
        self.nbWin = 0
        return -60

    def equal(self):
        self.nbWin = 0
        return -10

    def appendArrayOfWinVectors(self, arrayOfVectors):
        if self.winGameVectors in arrayOfVectors:
            print("exists")
            return
        else:
            self.winGameVectors.append(arrayOfVectors)

    def contains(self, toFind):
        i = 0
        for game in self.winGameVectors:
            contains = True
            for find in [toFind]:
                if find not in game:
                    contains = False
                    break
            if contains:
                gameRound = game[game.index(toFind)+1]
                index = 0
                while index < len(gameRound):
                    if gameRound[index] != toFind[index]:
                        print(index)
                        return index
                    index += 1
            i += 1
        return None