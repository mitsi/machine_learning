from tkinter import *
from PIL import Image,ImageDraw
from sklearn.metrics import accuracy_score,f1_score
import os

from sklearn import svm
from sklearn import datasets
import numpy as np


os.system('xset r on')
width = 100
height = 100
center = height//2
white = (255, 255, 255)
green = (0,128,0)

def dessin_rond(event):
    global root, canvas

    x = root.winfo_pointerx() - root.winfo_rootx()
    y = root.winfo_pointery() - root.winfo_rooty()
    canvas.create_oval(x, y, (x + 1), (y + 1), fill="black", width=15)
    draw.ellipse([x, y, (x + 1), (y + 1)], fill="black")

def save(event):
    global image1, canvas, clf, draw
    wpercent = (8 / float(image1.size[0]))
    hsize = int((float(image1.size[1]) * float(wpercent)))
    img = image1.resize((8, hsize), Image.ANTIALIAS)

    np_list = np.asarray(img.convert('L')).copy()
    np_list[np_list < 250] = 10  # Black
    np_list[np_list >= 250] = 0  # White
    list = []
    i=0
    while i < len(np_list):
        y=0
        while y<len(np_list[i]):
            list.append(np_list[i][y])
            y+=1
        i+=1

    print("The number:", clf.predict(np.asarray(list))[0])
    Image.fromarray(np_list).save("my_drawing.bmp")
    canvas.delete("all")
    image1 = Image.new("RGB", (width, height), white)
    draw = ImageDraw.Draw(image1)

digits = datasets.load_digits()
clf = svm.SVC(gamma=0.001, C=100.)
clf.fit(digits.data[:-1], digits.target[:-1])

print("accuracy:",accuracy_score(digits.target[:-1], clf.predict(digits.data[:-1])))
print("f1 score:", f1_score(clf.predict(digits.data[:-1]), digits.target[:-1], average='macro'))

root = Tk()
canvas = Canvas(root, width=width, height=height)
image1 = Image.new("RGB", (width, height), white)
draw = ImageDraw.Draw(image1)
canvas.bind("<Button-1>", save)
canvas.bind("<KeyPress>", dessin_rond)
canvas.pack()
canvas.focus_set()
root.mainloop()